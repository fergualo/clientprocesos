package com.edu.uclm.esi.procesos.client.model;

public class Person {
	private String dni;
	private String name;
	private String surname;
	private String email;

	public Person() {

	}

	public Person(String dni, String name, String surname, String email) {
		this.dni = dni;
		this.name = name;
		this.surname = surname;
		this.email = email;
	}

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

}
