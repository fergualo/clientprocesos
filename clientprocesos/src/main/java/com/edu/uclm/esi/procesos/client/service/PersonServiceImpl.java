package com.edu.uclm.esi.procesos.client.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Service;

import com.edu.uclm.esi.procesos.client.model.Person;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.json.JSONConfiguration;

@Service
public class PersonServiceImpl implements PersonService {

	private String URI = "http://localhost:8080/serverprocesos/person";

	@Override
	public void savePerson(Person person) {
		String url = URI + "/create";
		ClientConfig clientConfig = new DefaultClientConfig();
		clientConfig.getFeatures().put(JSONConfiguration.FEATURE_POJO_MAPPING, Boolean.TRUE);
		Client client = Client.create(clientConfig);
		WebResource webResource = client.resource(url);

		ClientResponse response = webResource.accept("application/json").type("application/json")
				.post(ClientResponse.class, person);
	}

	@Override
	public void deletePerson(Person person) {
		String url = URI + "/delete";
		ClientConfig clientConfig = new DefaultClientConfig();
		clientConfig.getFeatures().put(JSONConfiguration.FEATURE_POJO_MAPPING, Boolean.TRUE);
		Client client = Client.create(clientConfig);
		WebResource webResource = client.resource(url);

		ClientResponse response = webResource.accept("application/json").type("application/json")
				.post(ClientResponse.class, person);
	}

	@Override
	public void updatePerson(Person person) {
		String url = URI + "/update";
		ClientConfig clientConfig = new DefaultClientConfig();
		clientConfig.getFeatures().put(JSONConfiguration.FEATURE_POJO_MAPPING, Boolean.TRUE);
		Client client = Client.create(clientConfig);
		WebResource webResource = client.resource(url);
		@SuppressWarnings("unused")
		ClientResponse response = webResource.accept("application/json").type("application/json")
				.post(ClientResponse.class, person);
	}

	@Override
	public List<Person> getAllPerson() {
		String url = URI + "/get";
		ClientConfig clientConfig = new DefaultClientConfig();
		clientConfig.getFeatures().put(JSONConfiguration.FEATURE_POJO_MAPPING, Boolean.TRUE);
		Client client = Client.create(clientConfig);
		WebResource webResource = client.resource(url);
		ClientResponse response = webResource.accept("application/json").type("application/json")
				.get(ClientResponse.class);
		List<Person> listPerson = new ArrayList<Person>();

		ObjectMapper mapper = new ObjectMapper();

		try {
			listPerson = Arrays.asList(mapper.readValue(response.getEntityInputStream(), Person[].class));

		} catch (JsonParseException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return listPerson;
	}

	@Override
	public Person getPerson(String id_person) {
		String url = URI + "/get" + id_person;
		ClientConfig clientConfig = new DefaultClientConfig();
		clientConfig.getFeatures().put(JSONConfiguration.FEATURE_POJO_MAPPING, Boolean.TRUE);
		Client client = Client.create(clientConfig);
		WebResource webResource = client.resource(url);
		ClientResponse response = webResource.accept("application/json").type("application/json")
				.get(ClientResponse.class);

		Person person = response.getEntity(Person.class);

		return person;
	}

}
