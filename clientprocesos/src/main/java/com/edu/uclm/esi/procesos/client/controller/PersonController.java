package com.edu.uclm.esi.procesos.client.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.edu.uclm.esi.procesos.client.model.Person;
import com.edu.uclm.esi.procesos.client.service.PersonService;

@Controller
public class PersonController {

	@Autowired
	private PersonService personService;

	public PersonService getPersonService() {
		return personService;
	}

	public void setPersonService(PersonService personService) {
		this.personService = personService;
	}

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public ModelAndView InitialPage() {
		ModelAndView model = new ModelAndView();
		model.setViewName("initial");
		return model;
	}

	@RequestMapping(value = "showPerson", method = RequestMethod.GET)
	public ModelAndView showPersonPage() {
		ModelAndView model = new ModelAndView();
		model.addObject("person", new Person());
		List<Person> listperson = this.personService.getAllPerson();
		model.addObject("listPerson", listperson);
		model.setViewName("showPerson");
		return model;
	}
}
